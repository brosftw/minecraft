# Introduction
This is a container which runs Minecraft Spigot or Minecraft CraftBukkit Server.  
For more Information go to:  
* [GetBukkit.org](https://getbukkit.org)  
* [SpigotMC.org](https://www.spigotmc.org/)  

# Supported TAGS
Tags are based on the Craftbukkit/Spigot version!  
`test` [Test Branch](https://bitbucket.org/brosftw/minecraft/src/test/?at=test)  
`1.12` `latest` [1.12 Dockerfile](https://bitbucket.org/brosftw/minecraft/src/1.12/?at=1.12)  
`1.11` [1.11 Dockerfile](https://bitbucket.org/brosftw/minecraft/src/1.11/?at=1.11)  

---
## Usage
Basic start of the container.
*Notice: You need to agree the EULA from Mojang.  
This can be done automatically with an environment variable.*
```
docker run --name="my-minecraft-server" brosftw/minecraft:latest
```
### Persistant Storage
To map a persistant Storage add the `-v` parameter.  
For more information contact the docker guidelines!
```
docker run -v /my/persistant/directory/path:/mc-server --name="my-minecraft-server" brosftw/minecraft:latest
```

### Accepting the EULA
This Container provides the abillity to automatically accept the EULA.  
For this you need to set the enviromnet variable `EULA`. This can be done with the `-e` parameter.  
For more information contact the docker guidelines!
```
docker run -e EULA=true --name="my-minecraft-server" brosftw/minecraft:latest
```

### Tweaking Memory
A Minecraft Server eats lot of memory. With these enviroment variable you can limit this trough Java!
*Notice: This memory options didnt affect the docker memory limitations. Please contact the docker guidlines for more information about [Limit a container's resources](https://docs.docker.com/engine/admin/resource_constraints/)!*

For this you need to set the enviromnet variable `EULA`. This can be done with the `-e` parameter.
For more information contact the docker guidelines!
```
docker run -e MINMEMORY=2G -e MAXMEMORY=4G --name="my-minecraft-server" brosftw/minecraft:latest
```

### Access the container from outside
You can make your container available from outside. To do this use the `-p` option from docker or the docker networking!
```
docker run -p 25565:25565 --name="my-minecraft-server" brosftw/minecraft:latest
```


### All in ONE
Here we have the all in ONE docker container command!
The easyest way is to create the container first and start it after!
```
docker create \
-e EULA=true \
-p 25565:25565 \
-e MINMEMORY=512M \
-e MAXMEMORY=1G \
-v /my/persistant/directory/path:/mc-server \
--name="my-minecraft-server" \
brosftw/minecraft:latest

docker start my-minecraft-server
```
---
## Enviroment Variables
| ENV Variable | Default Value | Possible Values |
| ------------- |:-------------:| -----:|
| MINMEMORY | 512M | Memory in `M` or `G` |
| MAXMEMORY | 1G | Memory in `M` or `G` |
| SERVERTYPE | spigot | `spigot` or `craftbukkit` |
| EULA| false | `false` or `true` |
| UID | 1000 | Any possible UID |
| GID | 1000 | Any possible GID |

---
## Volumes
* /buildtools: 
 * craftbukkit-X.XX.jar
 * spigot-X.XX.jar

* /mc-server: 
 * banned-ips.json
 * banned-players.json
 * bukkit.yml
 * commands.yml
 * eula.txt
 * help.yml
 * logs
 * ops.json
 * permissions.yml
 * plugins
 * server.properties
 * spigot-1.12.jar
 * spigot.yml
 * usercache.json
 * whitelist.json
 * world
 * world_nether
 * world_the_end


---
# Contribution guidelines #

* Test the Containers
* Contact the [How to contribute](Link URL)
* Open a [BitBucket Issue](https://bitbucket.org/brosftw/minecraft/issues?status=new&status=open)

### Who do I talk to? ###

* Open an Issue @brosftw

