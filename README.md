# Introduction
This is a container which runs Minecraft Spigot or Minecraft CraftBukkit Server.  
For more Information go to:  
* [GetBukkit.org](https://getbukkit.org)  
* [SpigotMC.org](https://www.spigotmc.org/)  

# Supported TAGS
Tags are based on the Craftbukkit/Spigot version!  
`1.12.1` `latest` [1.12.1 Dockerfile](https://git.brosftw.ch/BrosFTW/Minecraft/src/1.12.1)  
`1.12` [1.12 Dockerfile](https://git.brosftw.ch/BrosFTW/Minecraft/src/1.12)  
`1.11` [1.11 Dockerfile](https://git.brosftw.ch/BrosFTW/Minecraft/src/1.11)  

---
## Usage
Basic start of the container.
*Notice: You need to agree the EULA from Mojang.  
This can be done automatically with an environment variable.*
```
docker run --name="my-minecraft-server" brosftw/minecraft:latest
```
### Persistant Storage
To map a persistant Storage add the `-v` parameter.  
For more information contact the docker guidelines!
```
docker run -v /my/persistant/directory/path:/mc-server --name="my-minecraft-server" brosftw/minecraft:latest
```

### Accepting the EULA
This Container provides the abillity to automatically accept the EULA.  
For this you need to set the enviromnet variable `EULA`. This can be done with the `-e` parameter.  
For more information contact the docker guidelines!
```
docker run -e EULA=true --name="my-minecraft-server" brosftw/minecraft:latest
```

### Tweaking Memory
A Minecraft Server eats lot of memory. With these enviroment variable you can limit this trough Java!
*Notice: This memory options didnt affect the docker memory limitations. Please contact the docker guidlines for more information about [Limit a container's resources](https://docs.docker.com/engine/admin/resource_constraints/)!*

For this you need to set the enviromnet variable `EULA`. This can be done with the `-e` parameter.
For more information contact the docker guidelines!
```
docker run -e MINMEMORY=2G -e MAXMEMORY=4G --name="my-minecraft-server" brosftw/minecraft:latest
```

### Access the container from outside
You can make your container available from outside. To do this use the `-p` option from docker or the docker networking!
```
docker run -p 25565:25565 --name="my-minecraft-server" brosftw/minecraft:latest
```


### All in ONE
Here we have the all in ONE docker container command!
The easyest way is to create the container first and start it after!
```
docker create \
-e EULA=true \
-p 25565:25565 \
-e MINMEMORY=512M \
-e MAXMEMORY=1G \
-v /my/persistant/directory/path:/mc-server \
--name="my-minecraft-server" \
brosftw/minecraft:latest

docker start my-minecraft-server
```
---
## Enviroment Variables
| ENV Variable | Default Value | Possible Values |
| ------------- |:-------------:| -----:|
| MINMEMORY | 512M | Memory in `M` or `G` |
| MAXMEMORY | 1G | Memory in `M` or `G` |
| SERVERTYPE | spigot | `spigot` or `craftbukkit` |
| EULA| false | `false` or `true` |
| UID | 1000 | Any possible UID |
| GID | 1000 | Any possible GID |

---

## Plugins

To install Plugins go to the folder where your docker container is.
Got to the Plugins Folder
```
cd /my/persistant/directory/path/plugins
```

Now copy your Craftbukkit or Spygod plugins into the folder and restart the container.

```
docker restart <Containername>
```

In the plugins folder there is a new folder where the configuration files are located.

---

## How to Upgrade your Minecraft server?

To upgrade your Minecraft Server you just have to pull and build the latest docker image and remove the old docker server.

```
docker rm <Containername>

docker pull brosftw/minecraft:latest

```
Start building your Minecraft server by checking the All in One Part of the manual.

*Important
All Plugins must be updated and comatible to the new version of Spygod or Craftbukkit

---
## Volumes
* /buildtools: 
 * craftbukkit-X.XX.jar
 * spigot-X.XX.jar

* /mc-server: 
 * banned-ips.json
 * banned-players.json
 * bukkit.yml
 * commands.yml
 * eula.txt
 * help.yml
 * logs
 * ops.json
 * permissions.yml
 * plugins
 * server.properties
 * spigot-X.XX.jar
 * spigot.yml
 * usercache.json
 * whitelist.json
 * world
 * world_nether
 * world_the_end


---
# Contribution guidelines #

* Test the Containers
* Contact the [How to contribute](Link URL)
* Open a [Issue](https://git.brosftw.ch/BrosFTW/Minecraft/issues/new)

### Who do I talk to? ###

* Open an Issue @git.brosftw.ch

